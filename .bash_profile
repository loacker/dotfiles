# ~/.bash_profile

# Source global definitions
[[ -f /etc/profile ]] && . /etc/profile


# Source global definitions
GLOBAL_BASH=$(echo $( (ls /etc/bashrc || ls /etc/bash.bashrc) 2> /dev/null))
[[ -f ${GLOBAL_BASH} ]] && . ${GLOBAL_BASH}

# This file is sourced by bash for login shells.  The following line
# runs your .bashrc and is recommended by the bash info pages.
[[ -f ~/.bashrc ]] && . ~/.bashrc


# Manage the OpenSSH rsa key on login
if [ "$EUID" != "0" ] || [ "$USER" != "root" ]; then
    [[ -f `which keychain` ]] > /dev/null 2>&1
    if [[ $? == 0 ]]; then
        eval `keychain --eval --nogui --noask --systemd --agents ssh -Q -q id_rsa`
    fi
fi


# Start or reconnect to a screen session
if [ -n "$SSH_CONNECTION" ] && [ -z "$SCREEN_EXIST" ]; then
    export SCREEN_EXIST=1
    if [[ $(screen -ls | awk '/remote/ {print $2}') != "(Attached)" ]]; then
        screen -RD remote
    else
        screen -r remote
    fi 
fi


# Start an X session
if systemctl -q is-active graphical.target && [[ $(id -u) != "0" ]] && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
      exec startx &> /dev/null
fi


# vim: set ts=8 sw=4 sts=4 tw=79 ff=unix ft=sh et ai :
