# ~/.bashrc

# Source alias definitions
[[ -f ~/.bash_aliases ]] && . ~/.bash_aliases
# Source some bash functions
[[ -f ~/.bash_functions ]] && . ~/.bash_functions

# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
[[ $- != *i* ]] && return

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# don't put duplicate lines in the history - ignoredups and ignorespace
# and erase duplicates
HISTCONTROL=ignoreboth:erasedups

# don't put this command in history
#HISTIGNORE=ls:"ls -altr":"ls -alt":"ls -al":pwd:exit:clear:history
HISTIGNORE="ls":"ls -alt":"ls -al":"ls -altr":pwd:exit:clear:history

# set the number of commands to remember in the history (default 500) 
# for other see the man page bash(1)
HISTSIZE=1000000000000
HISTFILESIZE=1000000000000
#HISTFILE=~/.history
#HISTTIMEFORMAT="[%F %T] "
HISTTIMEFORMAT="%Y-%m-%d %H:%M:%S "

# append to the history file, don't overwrite it
shopt -s histappend

# Force prompt to write history after every command.
#PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"
# Better solution to above from http://bit.ly/1Ga1bPx
history(){
	sync_history
	builtin history "$@"
}

sync_history(){
	builtin history -a
        HISTFILESIZE=$HISTFILESIZE
        HISTCONTROL=$HISTCONTROL
        builtin history -c
        builtin history -r
}

PROMPT_COMMAND="sync_history; $PROMPT_COMMAND"

# Disable xon/xoff (^]+s) for use the forward-seach-history (emacs shell mode)
# feature or change the key bindings to use ^]+t
stty -ixon
#bind "\C-t":forward-search-history

# Set dircolors
eval `dircolors ~/.dircolors`

# Color escape sequence
C_RESET='\033[0m'
C_BLACK='\033[0;30m'
C_RED='\033[0;31m'
C_GREEN='\033[0;32m'
C_YELLOW='\033[0;33m'
C_BLUE='\033[0;34m'
C_MAGENTA='\033[0;35m'
C_CYAN='\033[0;36m'
C_WHITE='\033[0;37m'

C_BOLD_RESET='\033[1m'
C_BOLD_BLACK='\033[1;30m'
C_BOLD_RED='\033[1;31m'
C_BOLD_GREEN='\033[1;32m'
C_BOLD_YELLOW='\033[1;33m'
C_BOLD_BLUE='\033[1;34m'
C_BOLD_MAGENTA='\033[1;35m'
C_BOLD_CYAN='\033[1;36m'
C_BOLD_WHITE='\033[1;37m'

# Set Colorful PS1
# Attribute codes: 00=none 01=bold 04=underscore 05=blink 07=reverse 08=concealed
# Text color codes: 30=black 31=red 32=green 33=yellow 34=blue 35=magenta 36=cyan 37=white
# Define an alias for the color - remember to escape with \[ at begin and \] at the end or 
# with the equivalent octal code \001 and \002, the octal code \033 can be substituted by '\e'.
# If you don't escape the prompt break the newline like inserting a carriage return 

#if [[ ${TERM} == xterm-256color || ${TERM} == xterm ]]; then
#    PS1="\[${C_BLACK}\][\u@\h][\t][\w]\n\[${C_RESET}\]\\$ "
#elif [[ (${TERM} == rxvt-unicode-256color) || ($TERM == screen-256color) || ($TERM == alacritty) || ($TERM == screen) ]]; then
#    PS1="\[${C_GREEN}\][\u@\h]\[${C_MAGENTA}\][\t]\[${C_WHITE}\][\w]\n\[${C_RESET}\]\\$ "
#fi

# Colorful/dynamic virtualenv
_virtualenv_ps1(){
#if [[ ${TERM} == xterm-256color || ${TERM} == xterm ]]; then
#    printf "%b" "${C_BLACK}${VIRTUAL_ENV:+[${VIRTUAL_ENV##*/}]}${C_RESET}"
#elif [[ (${TERM} == rxvt-unicode-256color) || ($TERM == screen-256color) || ($TERM == alacritty) || ($TERM == screen) ]]; then
#    printf "%b" "${C_CYAN}${VIRTUAL_ENV:+[${VIRTUAL_ENV##*/}]}${C_RESET}"
#fi
if [[ ${TERM} == xterm-256color || ${TERM} == xterm ]]; then
    printf "%b" "${C_BLACK}${VIRTUAL_ENV_PROMPT:+[${VIRTUAL_ENV_PROMPT:1:-2}]}${C_RESET}"
elif [[ (${TERM} == rxvt-unicode-256color) || ($TERM == screen-256color) || ($TERM == alacritty) || ($TERM == screen) ]]; then
    printf "%b" "${C_CYAN}${VIRTUAL_ENV_PROMPT:+[${VIRTUAL_ENV_PROMPT:1:-2}]}${C_RESET}"
fi
}

# Colorful/dynamic git branch
_git_ps1(){
    if [[ $(ls -d .git --color=none 2> /dev/null) == ".git" ]]; then
        git status 2> /dev/null | grep -Ei 'nothing to commit' > /dev/null 2>&1
        if [ "$?" -eq "0" ]; then
            if [[ ${TERM} == xterm-256color || ${TERM} == xterm ]]; then
                printf "%b" "${C_BLACK}[$(git branch 2> /dev/null \
                    | sed -e '/^\s\+/d' -e 's/^*\s//g')]${C_RESET}"
            elif [[ (${TERM} == rxvt-unicode-256color) || ($TERM == screen-256color) || ($TERM == alacritty) || ($TERM == screen) ]]; then
                printf "%b" "${C_YELLOW}[$(git branch 2> /dev/null \
                    | sed -e '/^\s\+/d' -e 's/^*\s//g')]${C_RESET}"
            fi
        else 
            printf "%b" "${C_RED}[$(git branch 2> /dev/null \
                | sed -e '/^\s\+/d' -e 's/^*\s//g')]${C_RESET}"
        fi
    else
        exit 0
    fi
}

# Extend PS1 with colorful/dynamic git branch and virtualenv and update when term got resized
_check_columns_ps1(){
    if [[ ${COLUMNS} -lt "90" ]]; then
        if [[ ${TERM} == xterm-256color || ${TERM} == xterm ]]; then
            export PS1="\n[\u@\h][\t][\W]\n\\$ "
        elif [[ (${TERM} == rxvt-unicode-256color) || ($TERM == screen-256color) || ($TERM == alacritty) || ($TERM == screen) ]]; then
            export PS1="\n\[${C_GREEN}\][\u@\h]\[${C_MAGENTA}\][\t]\[${C_WHITE}\][\W]\n\[${C_RESET}\]\\$ "
        fi
    else
        if [[ ${TERM} == xterm-256color || ${TERM} == xterm ]]; then
            export PS1="[\u@\h][\t][\w]\n\\$ "
        elif [[ (${TERM} == rxvt-unicode-256color) || ($TERM == screen-256color) || ($TERM == alacritty) || ($TERM == screen) ]]; then
            export PS1="\[${C_GREEN}\][\u@\h]\[${C_MAGENTA}\][\t]\[${C_WHITE}\][\w]\n\[${C_RESET}\]\\$ "
        fi
    fi
    [[ "$-" != "*i*" ]] && PS1='\n$(_virtualenv_ps1)$(_git_ps1)'$PS1
}

PROMPT_COMMAND="_check_columns_ps1;$PROMPT_COMMAND"

# Set default gtk theme
# gsettings set org.gnome.desktop.interface gtk-theme Adwaita
# gsettings set org.gnome.desktop.interface color-scheme prefer-dark
export GTK_THEME=Adwaita:dark

# Less colored output
#export LESSOPEN='|pygmentize -g %s'
#export LESS=RiXj5
export LESS="R"

# LIBVIRT Default URI
#export LIBVIRT_DEFAULT_URI=qemu:///system

# Vagrant default provider
export VAGRANT_DEFAULT_PROVIDER=libvirt

# OpenStack client
export OS_CLOUD=openstack
#if [ -f $HOME/.local/share/bash-completion/completions/openstack ]; then
#    # mkdir -p $HOME/.local/share/bash-completion/completions/
#    # openstack complete > $HOME/.local/share/bash-completion/completions/openstack
#    . $HOME/.local/share/bash-completion/completions/openstack
#    #complete -C /usr/bin/openstack openstack
#fi

# Rootless Docker
export DOCKER_HOST=unix://$XDG_RUNTIME_DIR/docker.sock

# cURL CA Bundle  
#export CURL_CA_BUNDLE="/etc/ca-certificates/extracted/ca-bundle.trust.crt"

# Colored manpages with LESS
export MANPAGER="less -R --use-color -Dd+r -Du+b"

# BASH Completion Aliases
if [ -f /usr/share/bash-complete-alias/complete_alias ]; then
    . /usr/share/bash-complete-alias/complete_alias
fi

# Disable AT-SPI D-Bus
export NO_AT_BRIDGE=1

# k8s PS1
#source '/opt/kube-ps1/kube-ps1.sh'
#PS1='[\u@\h \W $(kube_ps1)]$ '

## Go workspace
export GOPATH="$HOME/.go"
export GOBIN="$HOME/.go/bin"
#export GOROOT="$HOME/.go"

# direnv config
if command -v direnv 1>/dev/null 2>&1; then
    eval "$(direnv hook bash)"
fi

# glow completion
if command -v glow 1>/dev/null 2>&1; then
    source <(glow completion bash)
fi

# tfenv setup
if command -v tfenv 1>/dev/null 2>&1; then
    eval "$(tfenv init -)"
fi

# tfenv terraform completion
if command -v terraform 1>/dev/null 2>&1; then
    complete -C $HOME/.tfenv/bin/terraform terraform
fi

# nb notebook setup
export NB_DIR=$HOME/notes

## User specific PATH variables
#export PATH="$HOME/bin:$HOME/.local/bin${PATH:+:${PATH}}"

# Append "$1" to $PATH when not already in.
prepend_path () {
    case ":$PATH:" in
        *:"$1":*)
            ;;
        *)
            PATH="$1:${PATH:+$PATH}"
    esac
}

# Prepend additional default paths
prepend_path "$HOME/.local/bin"
prepend_path "$HOME/bin"
prepend_path "$HOME/.go/bin"
prepend_path "$HOME/.tfenv/bin"


# vim: set ts=8 sw=4 sts=4 tw=79 ff=unix ft=sh et ai :
