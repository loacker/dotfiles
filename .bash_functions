# List the function with "my-" prefix
my-functions-list() { declare -F | awk '!/my-functions-list/ && /my-/ {print $3}'; }

# Create and access a directory
my-mkdir() { /bin/mkdir -p "$@"; cd "$@"; }

# Manage  github repository
# Create a token with repo and delete permissions
# Set an include in the .gitconfig file and save the github credential in the
# .gitconfig.ignored file (set the exclusion in the .gitignore)
# git config --global include.path ~/.dotfiles/.gitconfig.ignored
# git config --file ~/.dotfiles/.gitconfig.ignored github.user <username>
# git config --file ~/.dotfiles/.gitconfig.ignored github.token <token>
my-github-createrepo() { 
    USER=$(git config github.user)
    TOKEN=$(git config github.token)
    [[ $1 == '' ]] && echo "ERROR: Missing repository name" && return 130
    curl -u "${USER}:${TOKEN}" https://api.github.com/user/repos \
         -d '{"name":"'${1}'"}' > /dev/null 2>&1
}

my-github-deleterepo() {
    USER=$(git config github.user)
    TOKEN=$(git config github.token)
    [[ $1 == '' ]] && echo "ERROR: Missing repository name" && return 130
    curl -X DELETE -u "${USER}:${TOKEN}" \
        https://api.github.com/repos/${USER}/${1} > /dev/null 2>&1
}

# Download MP3 with youtube-dl/yt-dlp
my-youtube-dl() {
    cd ~/music
    yt-dlp -x -i \
        --metadata-from-title '%(artist)s - %(title)s' \
        -o '%(artist)s - %(title)s - %(album)s.%(ext)s' \
        --add-metadata \
        --xattrs \
        --audio-format mp3 \
        --rm-cache-dir $@ 
    cd - > /dev/null
}

# AUR functions
my-aur-clone() {
    if [[ $(id -u) == 0 ]]; then
        echo "Running as root user, exit..."
    else
        for i in $@; do
            PROJECTS_DIR=$HOME/projects/aur
            STATUS=$(curl -X GET -skLI https://aur.archlinux.org/packages/$i | awk '/HTTP/ {print $2}')
            if [[ "$STATUS" -eq "200" ]]; then
                git clone https://aur.archlinux.org/$i.git $PROJECTS_DIR/$i > /dev/null 2>&1
            else
                echo -e "\n\033[0;31m"
                echo "NOTICE: AUR $i don't exist"
                echo -e "\033[0m"
            fi
        done
    
        unset array
        for dir in $@; do
            [[ -d $PROJECTS_DIR/$dir ]] && array+=("$dir")
        done
        lenght=${#array[@]}
        let lenght=$lenght-1
        
        echo -e "\033[0;34m"
        echo "AUR Cloned project"
        echo "=================="
        echo -e "\033[0m"
    
        echo -e "\033[0;33m"
        for i in $(seq 0 $lenght); do
            echo "$i) ${array[$i]}"
        done
        echo -e "\033[0m"
    
        read -p "=> Enter a number [Hit enter to select number 0]: " idx
        cd $PROJECTS_DIR/${array[$idx]}
        echo -e "\n"
        unset array
    fi
}

my-update-aur-pkgs-list() {
    if [[ $(id -u) == 0 ]]; then
        echo "Running as root user, exit..."
    else
        PROJECTS_DIR=$HOME/projects/aur
        for i in $(ls --color=none $PROJECTS_DIR); do
            cd $PROJECTS_DIR && git submodule add https://aur.archlinux.org/$i > /dev/null 2>&1
            EXIT_STATUS=$?
            cd $PROJECTS_DIR && git config submodule.$i.ignore all > /dev/null 2>&1
            if [[ $EXIT_STATUS == 1 ]]; then
                cd $PROJECTS_DIR && git submodule update
            fi 
            if [[ $EXIT_STATUS == 0 ]]; then
                echo "Adding module $i"
            fi
        done
            cd $PROJECTS_DIR && git status --short  | grep -qE '^A|^ ?'
            EXIT_STATUS=$?
            if [[ $EXIT_STATUS == 0 ]]; then
                read -p "=> Modules were added, would you like to commit: [yes/no] " value
                case $value in
                    yes|y)
                        MODULES_LIST=$(git status --short | grep -E --color=none '^A|^ ?' | awk '{printf $2" " }')
                        cd $PROJECTS_DIR && git add $MODULES_LIST && git commit -m "ADD modules $MODULES_LIST $(date +%d%m%Y_%H%M)"
                        read -p "=> Would you like to push this commit: [yes/no] " value
                        case $value in
                            yes|y)
                                cd $PROJECTS_DIR && git push --force --all
                            ;;
                            no|n)
                            ;;
                        esac
                    ;;
                    no|n)
                    ;;
                esac
            fi
    fi
}


# Check packages version in archive
my-archive-search() {
    PKGNAME=$1
    PKGINITIAL=${PKGNAME::1}
    URL="https://archive.archlinux.org/packages/$PKGINITIAL/$PKGNAME"
    PKGLIST=$(curl -sL $URL | sed -e 's/.*">//g' -e 's/<\/.*$//g' -e 's/<.*//g' -e 's/\.\.\///g' -e '/^$/d')
    for i in $PKGLIST; do
        echo $URL/$i
    done
}

# Some useful network functions
my-ip() { echo $(curl -s ipinfo.io/ip); }
my-country() { echo $(curl -s ipinfo.io/country); }
my-public-network-info() { curl -s ipinfo.io | jq '{
    "ip": .ip,
    "hostname": .hostname,
    "country": .country,
    "city exit node": .city,
    }';
}

# Stop blank/unblank
my-disable-dpms() { xset s off -dpms; }
my-enable-dpms() { xset s on dpms 60 0 0; }

# Show if a reboot/restart is necessary
my-needrestart(){
    deleted_libs=(`lsof -n +c 0 2> /dev/null | grep --color=none 'DEL.*lib' | awk '{ print $NF }' | sort -u`)
    [[ -n $deleted_libs ]] && echo -e "\nUpdated library:\n"
    [[ -n $deleted_libs ]] && for libs in ${deleted_libs[@]}; do
        pacman -Qo --color=never $libs 2>/dev/null
    done | awk '!a[$5]++ {system("tac /var/log/pacman.log | grep -m1 "$5"")}'

    running_apps=(`lsof -n +c 0 2> /dev/null | grep --color=none 'DEL.*lib' | awk '{ print $2 }' | sort -u`)
    [[ -n $running_apps ]] && echo -e "\nApps that need a restart:\n"
    [[ -n $running_apps ]] && ps --noheadings -o cmd -p ${running_apps[@]}| awk '{!apps[$1]++}; END {for(key in apps){system("basename "key)}}'

    running_kernel=$(uname -r)
    installed_kernel=$(ls --color=none /lib/modules)
    [[ $(basename ${running_kernel}) == ${installed_kernel} ]]
    [[ $(echo $?) != 0 ]] && echo -e "\nSystem need a reboot - kernel is installed but not loaded, installed ${installed_kernel}, running ${running_kernel}\n"
}

# Connect a bluetooth device
my-bluetooth() {
    connect(){
        DEFAULT_DEVICE='SoundCore 2'
        [[ $(systemctl is-active bluetooth.service) != "active" ]] && sudo systemctl start bluetooth
        sleep 1
        ORIG_IFS="$IFS"
        IFS=$'\n'
        devices=($(bluetoothctl devices | grep -v $(hostnamectl hostname)))
        default_device_mac=$(bluetoothctl devices | grep -v $(hostnamectl hostname)| awk '/'${DEFAULT_DEVICE}'/ {print $2}')
        echo && for ((idx=0; idx < ${#devices[@]}; idx++)); do
            name=$(echo "${devices[idx]}" | cut -d" " -f3-)
            echo "$idx" "$name"
        done
        echo && read -p "Select a device you wish to connect (Default to ${DEFAULT_DEVICE}): " idx
        if [[ -n "$idx" ]]; then
            idx=${idx}
            bluetoothctl connect $(echo "${devices[idx]}" | cut -d" " -f2) >/dev/null
        else
            bluetoothctl connect $(echo "${default_device_mac}" | cut -d" " -f2) >/dev/null
        fi
        IFS="$ORIG_IFS"
    }
    
    disconnect(){
        if [[ $(systemctl is-active bluetooth.service) == "active" ]]; then
            connected_device=$(bluetoothctl info | awk '/Device/ {print $2}')
            [[ "$(echo ${connected_device})" == "(null)" ]] && bluetoothctl disconnect ${connected_device}
            sudo systemctl stop bluetooth
        fi
    }
    
    case $1 in
        connect|c|conn) connect ;;
        disconnect|d|disc) disconnect ;;
        *) echo "${FUNCNAME[0]} [connect|c|conn] or [disconnect|d|disc]" ;;
    esac
}

# Weather forecast
my-weather-forecast() {
    query="curl https://wttr.in"
    [[ -n "$1" ]] && $(echo $query/$1) || $(echo $query)
}

# Timezone converter
my-timezone-converter() {
    funcname=${FUNCNAME[0]}
    args=($@)
    timezone=${args[0]}
    date=${args[@]:1}

    usage(){
        echo -e "\nUsage examples"
        echo -e "\t$funcname timezone date"
        echo -e "\t$funcname '-l|--list|--list-timezones' list timezones"
        echo -e "\t$funcname America/Vancouver 07:00am 3 oct"
        echo -e "\t$funcname America/Vancouver 07:00am next fri"
        echo -e "\t$funcname '-h|--help' show usage\n"
    }

    case $1 in
        -h|--help) usage ;;
        -l|--list|--list-timezones) timedatectl list-timezones ;;
        "") usage;;
        *) date --date='TZ="'$timezone'" '"$date"'' ;;
    esac
}

# Send file to NoteAir3C
my-send2boox() { curl -F "file=@$1" "http://boox.station:8085/api/storage/upload" | jq ; }
my-get2boox() { curl "http://boox.station:8085/api/storage/file?args=/storage/emulated/0/Books/$1" -o $1 ; }

# Upload files content on 0x0.st
my-pastebin() {
    if [ -z "${1}" ]; then
        echo "Usage: ${FUNCNAME[0]} <filename>"
        return 1
    fi
    local filename="${1}"
    curl -F "file=@${filename}" https://0x0.st
}

# vim: set ts=4 sw=4 sts=4 tw=79 ff=unix ft=sh et ai :
