" ViM config

" Leader key - Default is "\"
" ---------------------------
"let mapleader="-"

" Enable Pathogen (default dir is .vim/bundle)
" --------------------------------------------
execute pathogen#infect()

" Force Vim to assume 256 colors
" ------------------------------
if &term == "screen-256color"
  set t_Co=256
endif

" Enable powerline
" ----------------
source ~/.vim/bundle/powerline/powerline/bindings/vim/plugin/powerline.vim

" Enable filetype plugins
" -----------------------
filetype plugin on

" Set the automatic indention per file type  
" -----------------------------------------
filetype plugin indent on

" Copy the previous indentation on autoindenting
" ----------------------------------------------
set copyindent

" Python specific settings
" ------------------------
autocmd filetype python set expandtab

" Set modeline on and set the size where looking for the
" parameters from top or bottom es: /* vim: ft=cpp:...*/
" ------------------------------------------------------
set modeline
set modelines=5

" Toggle paste (no autoindent) 
" ----------------------------
nnoremap <F12> :set invpaste paste?<CR>
set pastetoggle=<F12>

" Toggle number column
" --------------------
nnoremap <F11> :set invnumber number?<CR>

" Set number colors
" -----------------
set number
set numberwidth=6
highlight LineNr ctermfg=grey ctermbg=none
highlight CursorLineNR ctermbg=none ctermfg=white term=bold cterm=bold

" Set syntax highlight
" --------------------
syntax on

" Set Comment colors
" ------------------
highlight Comment ctermfg=grey

" Set backgroud colors
" --------------------
"set background=dark
set background=light

" Cursor position on status line
" ------------------------------
set ruler 

" Set status line colors
" ----------------------
set laststatus=2 " Full status line 
au InsertEnter * hi StatusLine ctermfg=darkred ctermbg=black
au InsertLeave * hi StatusLine ctermfg=white ctermbg=black
hi StatusLineNC ctermfg=darkblue

" Set vimdiff colors
" ------------------
if &diff
  syntax off
endif
highlight DiffAdd ctermfg=black ctermbg=green
highlight DiffChange ctermfg=black ctermbg=red
highlight DiffDelete ctermfg=black ctermbg=blue
highlight DiffText ctermfg=white ctermbg=red

" Enables cursor line position tracking
" -------------------------------------
set cursorline

" Removes the underline when cursor line position tracking is on
" --------------------------------------------------------------
highlight clear CursorLine
highlight CursorLine term=bold cterm=bold 

" Set popup menu height (completion menu c-x + c-o)
" -------------------------------------------------
set pumheight=3

" Nice chars when set list is on
" ------------------------------
set listchars=tab:>-

" Visualizing tabs
" ----------------
syntax match tab /\t/
highlight tab ctermfg=yellow ctermbg=red

" Visual selection color
" ----------------------
highlight Visual term=reverse
"highlight Visual ctermbg=white ctermfg=black

" Show command in the last line of the screen
" es. The leader key when pressed until the timeout left
" ------------------------------------------------------
set showcmd

" Set show matching parenthesis
" -----------------------------
set showmatch
highlight MatchParen ctermfg=black ctermbg=white

" Folding
" -------
set foldmethod=indent
set foldlevel=99

" pyflakes
" --------
let g:pyflakes_use_quickfix = 0

" pep8
" ----
let g:pep8_map='<leader>8'

" Persistent undo/redo
set undofile
set undodir=~/.vim/undodir

" Map for save a file with sudo privilege (sudo rules must exist)
" ---------------------------------------------------------------
noremap <Leader>W :w !sudo tee % > /dev/null

" VimCommander toggle
" -------------------
noremap <silent> <F2> :cal VimCommanderToggle()<CR>

" EasyBuffer toggle
" -----------------
map <F3> :EasyBuffer<CR> 

" TagList toggle
" --------------
map <silent> <F4> :TlistToggle<CR> 

" TaskList toggle
" ---------------
nnoremap <leader>t <Plug>TaskList
map tl :TaskList<CR>

" NERDTree toggle
" ---------------
map <C-n> :NERDTreeToggle<CR>

" Gundo plugin
" ------------
"map g :GundoToggle<CR>

" Vimwiki setup
"--------------
let g:vimwiki_list = [{'path': '~/notes/', 'syntax': 'markdown', 'ext': 'md'}]
let g:vimwiki_global_ext = 0
let g:vimwiki_ext2syntax = {}

" Set color scheme 
" ----------------
"colorscheme PaperColor
"colorscheme jellybeans
"colorscheme challenger_deep.vim

" Return to last edit position when opening files
" -----------------------------------------------
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

" Unconditional linewise or characterwise paste
" ---------------------------------------------
function! PasteJointCharacterwise(regname, pastecmd)
  let reg_type = getregtype(a:regname)
  call setreg(a:regname, '', "ac")
  exe 'normal "'.a:regname . a:pastecmd
  call setreg(a:regname, '', "a".reg_type)
  exe 'normal `[v`]J'
endfunction
nmap <Leader>p :call PasteJointCharacterwise(v:register, "p")<CR>
nmap <Leader>P :call PasteJointCharacterwise(v:register, "P")<CR>

function! Paste(regname, pasteType, pastecmd)
  let reg_type = getregtype(a:regname)
  call setreg(a:regname, getreg(a:regname), a:pasteType)
  exe 'normal "'.a:regname . a:pastecmd
  call setreg(a:regname, getreg(a:regname), reg_type)
endfunction
nmap <Leader>lP :call Paste(v:register, "l", "P")<CR>
nmap <Leader>lp :call Paste(v:register, "l", "p")<CR>
nmap <Leader>cP :call Paste(v:register, "v", "P")<CR>
nmap <Leader>cp :call Paste(v:register, "v", "p")<CR>

" Modeline
" --------
" From http://vim.wikia.com/wiki/Modeline_magic
" Append modeline after last line in buffer.
" Example for set "noet" with printf
" printf(" vim: set %set :", &expandtab ? '' : 'no')
function! AppendModeline()
  let l:modeline = printf(" vim: set ts=%d sw=%d sts=%d tw=%d ff=%s ft=%s et ai :",
        \ &tabstop, &shiftwidth, &softtabstop, &textwidth, &fileformat, &filetype)
  let l:modeline = substitute(&commentstring, "%s", l:modeline, "")
  call append(line("$"), l:modeline)
endfunction
nnoremap <silent> <Leader>ml :call AppendModeline()<CR>

function! PyAppendModeline()
  let l:pymodeline = printf("%s",
	\ '# vim: set ts=8 sw=4 sts=4 tw=79 ff=unix ft=python et ai :')
  call append(line("$"), l:pymodeline)
endfunction
nnoremap <silent> <Leader>pml :call PyAppendModeline()<CR>

" Default indentation and line-wrapping
" -------------------------------------
" lines longer than 79 columns will be broken
set textwidth=79
" operation >> indents 4 columns; << unindents 4 columns
set shiftwidth=4
" a hard TAB displays as 4 columns
set tabstop=4
" insert spaces when hitting TABs
set expandtab
" insert/delete 4 spaces when hitting a TAB/BACKSPACE
set softtabstop=4
" round indent to multiple of 'shiftwidth'
set shiftround
" align the new line indent with the previous line
set autoindent


" Some modeline example
" ---------------------
" # vi: ts=8 sw=4 sts=4 ai et tw=79
" # vim: set ts=8 sw=4 sts=4 ai et tw=79:
" # vim: tabstop=8:softtabstop=4:shiftwidth=4:expandtab:textwidth=79:autoindent

" N.B. Inserted five blank row otherwise vim will look for the above modeline





