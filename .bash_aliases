# My personal shell alias
#
# NOTE: Add alias completion with complete -F _complete_alias <alias>

# Sudo alias to workaround the use of aliases
alias sudo='sudo '

# Pacman aliases
alias pacman="pacman --color always"
alias pacclean="pacman -Qqqdt | ifne sudo pacman -Rcns -"
alias pacaurdiff="comm -13 <(ls -1 --color=none ~/projects/aur/ | sort) <(pacman -Qqm | sort)"
alias pacmultilib="comm -12 <(pacman -Qq | sort) <(pacman -Slq multilib | sort)"
alias pacetcchanges="pacman -Qii | awk '/^MODIFIED/ {print $2}'"
alias pacpkgspreview="pacman -Qq | fzf --preview 'pacman -Qil {}' --layout=reverse --bind 'enter:execute(pacman -Qil {} | less)'"
alias pacallpkgspreview="pacman -Slq | fzf --preview 'pacman -Si {}' --layout=reverse"
alias pacenabletestrepo="sed -i -e '71,71s/^#\[\(.*\)/\[\1/g' -e '72,72s/^#I\(.*\)/I\1/g' /etc/pacman.conf"
alias pacdisabletestrepo="sed -i -e '71,71s/^\[\(.*\)/#\[\1/g' -e '72,72s/^I\(.*\)/#I\1/g' /etc/pacman.conf"
alias paccachealluninstalled="paccache -ruk0"
alias paccacheonecopy="paccache -rk1"
alias sysupgrade="pacman -Syu --noconfirm"
complete -F _complete_alias sysupgrade

# Snapper aliases
alias snapperdeleteallroot="snapper list --columns number | grep -Evw '#|-|0' | xargs snapper delete"
alias snapperdeleteallhome="snapper -c home list --columns number | grep -Evw '#|-|0' | xargs snapper -c home delete"

# Update dotfiles
alias updotfiles="pushd ~/.dotfiles/ && git pull && popd"

# VIM alias
alias vi="vim"

# nmcli
alias ncs="nmcli connection show"
complete -F _complete_alias ncs
alias ndwl="nmcli device wifi list"
complete -F _complete_alias ndwl

# Music manager
alias ncmpcpp='[ -z $(pgrep mpd) ] && mpd && ncmpcpp || ncmpcpp'
alias vimpc='[ -z $(pgrep mpd) ] && mpd && vimpc || vimpc'

# Download entire websites
alias wd='wget -r --no-parent --reject "index.html*" $1'
complete -F _complete_alias wd

# Colored output aliases
alias ls='ls --color=always' 
alias grep='grep --color=always' 
alias ip='ip --color=auto'

# Webcam test
alias wcam-list='v4l2-ctl --list-devices'
alias wcam-integrated='mplayer tv:///'
alias wcam-external='mplayer tv:/// -tv device=/dev/video4'
complete -F _complete_alias wcam-list
complete -F _complete_alias wcam-integrated
complete -F _complete_alias wcam-external


# youtube-dl / yt-dlp
alias ydl='my-youtube-dl "$($(which xclip) -o 2>/dev/null)"'

# youtube-fzf
alias ytfzf='ytfzf -m -l -s -t --async-thumbnails --preview-side=right --fancy-subs --pages=7 --type=all'

# Translate aliases
alias tit='trans en:it "$@" "$(xclip -o)"'
alias titp='trans -b -p en:it "$@" "$(xclip -o)"'
alias ten='trans it:en "$@" "$(xclip -o)"'
alias tenp='trans -b -p it:en "$@" "$(xclip -o)"'

# Download torrent with magnet url
alias torrentdownload='transmission-cli "$(xclip -o 2>/dev/null)"'

# Show listen ports
alias openports="lsof -n -PiTCP -sTCP:LISTEN"
complete -F _complete_alias openports

# Bluetooth battery status
alias bt-bat-status="busctl get-property org.bluez /org/bluez/hci0/dev_30_53_C1_7E_A8_B7 org.bluez.Battery1 Percentage | awk '{print \$2\"%\"}'"
alias check-battery-status="upower -d | awk '/Device: / || /percentage:/'"
alias volume-restore="systemctl --user restart pipewire.service pipewire-pulse.service wireplumber.service"

# Process
alias psc="ps axwf -eo pid,user,cgroup,args"

# OpenStack Command Line Client
#alias openstack="openstack --debug $@ 2>>/tmp/openstack-debug.log"
#alias myopenstack="OS_TOKEN="" openstack --os-cloud personal $@ 2>>/tmp/myopenstack-debug.log"
#complete -F _complete_alias myopenstack

# vim: set ts=8 sw=8 sts=0 tw=0 ff=unix ft=sh et ai :
