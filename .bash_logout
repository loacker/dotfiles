# ~/.bash_logout

# Source global definitions
GLOBAL_BASH=$(echo $( (ls /etc/bash_logout || ls /etc/bash.bash_logout) 2> /dev/null))
[[ -f ${GLOBAL_BASH} ]] && . ${GLOBAL_BASH}

# Clear the screen for security's sake.
clear

# Kill the gpg-agent
#if [ -n "${GPG_AGENT_INFO}" ]; then
#  kill $(echo ${GPG_AGENT_INFO} | cut -d':' -f 2) >/dev/null 2>&1
#fi

